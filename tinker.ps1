# https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.security/convertto-securestring?view=powershell-7.3
$Username = [System.Environment]::GetEnvironmentVariable('RDXUser')
$RawPassword = [System.Environment]::GetEnvironmentVariable('RDXPassword')
$Password = ConvertTo-SecureString $RawPassword -AsPlainText -Force
$WebDAVUser = [System.Environment]::GetEnvironmentVariable('WebDAVUser')
$WebDAVPassword = [System.Environment]::GetEnvironmentVariable('WebDAVPassword')
$WebDAVFolderDataset = [System.Environment]::GetEnvironmentVariable('WebDAVFolderDataset')
$WebDAVFolderResults = [System.Environment]::GetEnvironmentVariable('WebDAVFolderResults')

# login with $Username@src.local
New-LocalUser -Name $Username -Password $Password
net localgroup "Remote Desktop Users" $Username /ADD
net localgroup "Remote Management Users" $Username /ADD

$UserCheck = Get-LocalUser
Write-Output "Local users: $UserCheck"

New-Item -Path "c:\" -Name "rdx" -ItemType "directory"
$outputMount = net use r: $WebDAVFolderDataset /user:$WebDAVUser $WebDAVPassword /Persistent:YES
Write-Output "Output mount: $outputMount"
$counter = 0
while((Get-ChildItem -Path 'r:\' -Recurse).Count -eq 0){
    $counter++
    if ($counter -gt 10) {
        Write-Output "Timed out waiting for mount"
        break
    }
    Write-Output "Waiting for mount... attempt: $counter"
    Start-Sleep -Seconds 30
}
$listOfFiles = Get-ChildItem -Path 'r:\' -Recurse
Write-Output "List of files on mount: $listOfFiles"
$outputCopy = robocopy r:\ c:\rdx\dataset /s
Write-Output "Output mount: $outputCopy"
$counter = 0
while((Get-ChildItem -Path 'c:\rdx\dataset' -Recurse).Count -eq 0){
    $counter++
    if ($counter -gt 10) {
        Write-Output "Timed out waiting for copy"
        break
    }
    Write-Output "Waiting for copy... attempt: $counter"
    Start-Sleep -Seconds 30
}
$listOfFilesDst = Get-ChildItem -Path 'c:\rdx\dataset' -Recurse
Write-Output "List of files destination: $listOfFilesDst"
net use r: /delete

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("c:\rdx\upload.url")
$Shortcut.TargetPath = $WebDAVFolderResults
$Shortcut.Save()

New-Item "c:\rdx\upload-password.txt" -ItemType File -Value $RawPassword
# Delete default shortcuts
Remove-Item -Path 'C:\Users\Public\Desktop\Google Chrome.lnk' -Force
Remove-Item -Path 'C:\Users\Public\Desktop\Microsoft Edge.lnk' -Force
# Create script to delete user shortcuts
$removeShortcutsScript = "C:\rdx\removeshortcuts.ps1"
New-Item $removeShortcutsScript
Add-Content -Path $removeShortcutsScript -Value "Remove-Item -Path 'C:\Users\$Username\Desktop\Google Chrome.lnk' -Force"
Add-Content -Path $removeShortcutsScript -Value "Remove-Item -Path 'C:\Users\$Username\Desktop\Microsoft Edge.lnk' -Force"

Copy-Item -Path "C:\Users\rsc\src-scripts\SetDefaultBrowser.exe" -Destination "C:\SetDefaultBrowser.exe"
$startupScript = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\setdefaultbrowser.cmd"
New-Item $startupScript -ItemType File
Add-Content -Path $startupScript -Value "@echo off"
Add-Content $startupScript -Value 'C:\SetDefaultBrowser.exe hklm "google chrome"'
Add-Content $startupScript -Value "pwsh.exe -ExecutionPolicy Unrestricted -WindowStyle Hidden $removeShortcutsScript"

$startupScript = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\webdavmount.cmd"
New-Item $startupScript -ItemType File
Add-Content -Path $startupScript -Value "@echo off"
